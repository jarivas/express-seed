If you want to use HTTPS then add your certificates in this folder.

The two files that you need are:

server.crt --> Server certificate WITHOUT passphase in .PEM format
server.key --> Server private key in .PEM format

If you need to convert certificates use always openssl tool. For samples on how to use it you can check:

https://www.sslshopper.com/ssl-converter.html

NOTE: Don't use a web to convert your certificates!! openssl is installed by default in any linux/macos distribution and can be easily installed in windows.


Read the below for a sample on how to create self signed certificates

https://matoski.com/article/node-express-generate-ssl/


Once you have the certificates. Change the /bin/www file to put your certificate names and if you want to enable the HTTP section in addition to the HTTPS.

