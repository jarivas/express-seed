

var expect = require("chai").expect,
    request = require("supertest"),
    base_url = "https://localhost:3000/",
    server  = require('../bin/www'),
    app = require('../app');

//allow to use self-signed certificates -- REMOVE WHEN GOING PRODUCTION!
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

describe("OBIEE Access tests", function(){

    var agent = request.agent();

    after(function() {
        // runs after all tests in this block
        console.log('Closing Server...');
        server.close();
    });

    describe("Index page is working", function() {
       it("returns status code 200", function(done){
           request(app)
               .get('/')
               .expect(200)
               .end(function(err, res){
                   if (err) return done(err);
                   done();
               });
       }) ;
    });


});